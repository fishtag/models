<?php
/*
Plugin Name: Models
Plugin URI: http://sputnikmobile.com/
Description: Models plugin
Version: 1.1
Author: the Sputnik Mobile team
Author URI: http://sputnikmobile.com/
Bitbucket Plugin URI: https://bitbucket.org/fishtag/models
Bitbucket Branch: master
*/

/*Some Set-up*/
define('MODELS_PLUGIN_DIR', WP_PLUGIN_URL . '/' . plugin_basename( dirname(__FILE__) ) . '/' );

/*-------------------------------------------------------------------------------*/
/*   Register Custom Post Types
/*-------------------------------------------------------------------------------*/
add_action( 'init', 'models_create_post_type' );
function models_create_post_type() {
  register_post_type( 'model',
    array(
      'labels' => array(
        'name' => 'Models',
        'singular_name' => 'Model',
        'add_new' => 'Add New',
        'add_new_item' => 'Add New Model',
        'edit' => 'Edit',
        'edit_item' => 'Edit Model',
        'new_item' => 'New Model',
        'view' => 'View',
        'view_item' => 'View Model',
        'search_items' => 'Search Models',
        'not_found' => 'No Models found',
        'not_found_in_trash' => 'No Models found in Trash',
        'parent' => 'Parent Model'
      ),
      'public' => true,
      'show_ui' => true,
      'publicly_queryable' => true,
      'exclude_from_search' => true,
      'has_archive' => false,
      'hierarchical' => false,
      'rewrite' => array( 'slug' => 'model' ),
      'capability_type' => 'page',
      'menu_icon' => 'dashicons-universal-access'
    )
  );
}